# PyQt5

## 1.使用designer绘制PyQt5 GUI 界面

1)可以使用Qt create制作`.ui`文件

2}也可以直接在Anaconda的安装目录中,找到anaconda安装目录下的`\Library\bin`里下的`designer.exe`双击打开,例如我这里在`D:\software\Anaconda3\Library\bin`中的`designer.exe`

## 2.将.ui文件转换为.py文件

命令行输入:

方法1:

`python -m PyQt5.uic.pyuic ui文件名.ui -o 生成py文件名.py`

例如:`python -m PyQt5.uic.pyuic demo.ui -o demo.py`

方法2:

`pyuic5 ui文件名.ui -o  生成py文件名.py`

例如:`pyuic5 demo.ui -o  demo.py`

