# python Selenium爬虫爬取京东搜索页信息

## 1.什么是 Selenium框架?

1.Selenium是一个用于Web应用程序测试的工具。直接运行在浏览器中,就像真正的用户在操作一样

2.支持的浏览器包括IE,火狐, Safari, Google Chrome, Opera等,覆盖平台多

3.自动化测试,js动态爬虫

## 2.环境配置

这里我使用的是火狐浏览器驱动geckodriver,如果你使用的是谷歌浏览器,你需要使用chromedriver.

下载地址:[geckodriver ](https://github.com/mozilla/geckodriver/releases)

注意这里的浏览器驱动版本要与浏览器版本对应,建议驱动下载最新版本,浏览器更新到最新版本

这里我下载的是[geckodriver-v0.29.1-win64.zip](https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-win64.zip)1.44 MB

解压后可以得到`geckodriver.exe`文件,将这个可执行文件添加的你的python安装目录下的`Scripts`文件夹中即可.若你使用的是anaconda,则需添加到`Anaconda3` 中的`Scripts`文件夹中

在cmd里输入`geckodriver` 你可以得到

`(crawler) C:\Users\HP>geckodriver
1630680105974   geckodriver     INFO    Listening on 127.0.0.1:4444`

出现这样的形式则表明安装成功.

下载`Selenium`库,使用python最常用命令`pip install Selenium`安装Selenium框架

## 3.代码编写

### 3.1 导入相关库

```python
# 导入库
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
```

### 3.2 模拟浏览器操作

```python
# 模拟浏览器访问京东页面,解决反爬虫
def spider(url, keyword):
    # 初始化浏览器
    driver = webdriver.Firefox()
    # 访问网址
    driver.get(url)
    # time.sleep(2) # 20
    try:
        driver.implicitly_wait(10)  # 隐式等待,确保节点完全加载出来,感受不到

        # 搜索
        # 定位搜索框
        input_tag = driver.find_element_by_id('key')
        # 模拟键盘输入关键字
        input_tag.send_keys(keyword)
        # 模拟按下回车键搜索
        input_tag.send_keys(Keys.ENTER)
        get_goods(driver)
    finally:
        pass
        # time.sleep(10)
        # # 关闭浏览器  总会执行
        # driver.close()
```

### 3.3 抓取需要的数据

```python
# 定位商品数据抓取
def get_goods(driver):
    try:
        # 定位到每一件商品
        goods = driver.find_elements_by_class_name('gl-item')
        # 商品名字 href 价格 评论
        for good in goods:
            p_name = good.find_element_by_css_selector('.p-name em').text.replace('\n', '')
            detail_url = good.find_element_by_tag_name('a').get_attribute('href')
            price = good.find_element_by_css_selector('.p-price i').text
            p_commit = good.find_element_by_css_selector('.p-commit a').text
            msg = """
            商品:%s
            链接:%s
            价格:%s
            评论:%s
            """ % (p_name, detail_url, price, p_commit)
            print(msg)
            # with open("")
    # 大量的数据
        # 定位到下一页
        button = driver.find_element_by_partial_link_text('下一页')
        # 模拟点击操作
        button.click()
        time.sleep(2)
        get_goods(driver)

    except Exception as e:
        print(e)
```

### 3.4 程序入口

```python
if __name__ == '__main__':
    spider('https://www.jd.com/', keyword='jk')
```

这里输入网址,keyword是搜索关键字

## 4.完整代码

```python
# 环境配置
"""
浏览器驱动 geckodriver
1630635571195   geckodriver     INFO    Listening on 127.0.0.1:4444
"""

# 导入库
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time


# 模拟浏览器访问京东页面,解决反爬虫
def spider(url, keyword):
    # 初始化浏览器
    driver = webdriver.Firefox()
    # 访问网址
    driver.get(url)
    # time.sleep(2) # 20
    try:
        driver.implicitly_wait(10)  # 隐式等待,确保节点完全加载出来,感受不到

        # 搜索
        # 定位搜索框
        input_tag = driver.find_element_by_id('key')
        # 模拟键盘输入关键字
        input_tag.send_keys(keyword)
        # 模拟按下回车键搜索
        input_tag.send_keys(Keys.ENTER)
        get_goods(driver)
    finally:
        pass
        # time.sleep(10)
        # # 关闭浏览器  总会执行
        # driver.close()


# 定位商品数据抓取
def get_goods(driver):
    try:
        # 定位到每一件商品
        goods = driver.find_elements_by_class_name('gl-item')
        # 商品名字 href 价格 评论
        for good in goods:
            p_name = good.find_element_by_css_selector('.p-name em').text.replace('\n', '')
            detail_url = good.find_element_by_tag_name('a').get_attribute('href')
            price = good.find_element_by_css_selector('.p-price i').text
            p_commit = good.find_element_by_css_selector('.p-commit a').text
            msg = """
            商品:%s
            链接:%s
            价格:%s
            评论:%s
            """ % (p_name, detail_url, price, p_commit)
            print(msg)
            # with open("")
    # 大量的数据
        # 定位到下一页
        button = driver.find_element_by_partial_link_text('下一页')
        # 模拟点击操作
        button.click()
        time.sleep(2)
        get_goods(driver)

    except Exception as e:
        print(e)


if __name__ == '__main__':
    spider('https://www.jd.com/', keyword='jk')

```

